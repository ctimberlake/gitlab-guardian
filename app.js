require('dotenv').config();
var fs = require('fs');
var http = require('http')
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));


var server = http.createServer((request, response) => {
    const chunks = [];
    request.on("data", (chunk) => {
      chunks.push(chunk);
    });
    request.on("end", () => {
      console.log("all parts/chunks have arrived");
      const data = Buffer.concat(chunks);
      console.log("Data: ", data);
    });
})

server.listen(process.env.HTTP_PORT, () => {
    console.log("Server is running on Port "+process.env.HTTP_PORT);
});